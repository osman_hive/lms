<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Exception;
use Validator;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $show_teachers = User::where('role', "3")
                ->get();
            if (!$show_teachers) {
                throw new Exception('No data available!');
            }

            return response()->json(array(
                'status' => true,
                'teachers' => $show_teachers,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $rules = array(
                'username'  => 'required',
                'password'     => 'required',
                'first_name'  => 'required',
                'last_name'   => 'required',
                'email'         => 'required',
                'phone'         => 'required',
                'address'       => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                throw new Exception('All fields are required');
            }

            $insert_teacher['username'] = $request->username;
            $insert_teacher['first_name'] = $request->first_name;
            $insert_teacher['last_name'] = $request->last_name;
            $insert_teacher['email'] = $request->email;
            $insert_teacher['phone'] = $request->phone;
            $insert_teacher['address'] = $request->address;
            $insert_teacher['role'] = "3";
            $insert_teacher['password'] = Hash::make($request->password);

            $create_teacher = User::create($insert_teacher);
            if (!$create_teacher) {
                throw new Exception('Create Teacher failed!');
            }

            return response()->json(array(
                'status' => true,
                'status_message' => "Teacher Create Successful!",
                'teacher' => $create_teacher,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $show_teacher = User::where('user_id', $id)
                ->first();
            if (!$show_teacher) {
                throw new Exception('Teacher doesnot exist!');
            }

            return response()->json(array(
                'status' => true,
                'teacher' => $show_teacher,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $rules = array(
                'username'  => 'required',
                'password'     => 'required',
                'first_name'  => 'required',
                'last_name'   => 'required',
                'email'         => 'required',
                'phone'         => 'required',
                'address'       => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                throw new Exception('All fields are required');
            }

            $update_teacher['username'] = $request->username;
            $update_teacher['first_name'] = $request->first_name;
            $update_teacher['last_name'] = $request->last_name;
            $update_teacher['email'] = $request->email;
            $update_teacher['phone'] = $request->phone;
            $update_teacher['address'] = $request->address;
            $update_teacher['password'] = Hash::make($request->password);

            $update = User::where('user_id', $id)
                ->update($update_teacher);
            if (!$update) {
                throw new Exception('Update Teacher failed!');
            }

            return response()->json(array(
                'status' => true,
                'status_message' => "Teacher Update Successful!",
                // 'teacher' => $update,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $delete_teacher = User::where('user_id', $id)
                ->delete();
            if (!$delete_teacher) {
                throw new Exception('Delete Teacher failed!');
            }

            return response()->json(array(
                'status' => true,
                'status_message' => "Teacher Delete Successful!",
                // 'teacher' => $delete_teacher,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }          
    }
}
