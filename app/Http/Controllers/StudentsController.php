<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Exception;
use Validator;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $show_students = User::where('role', "4")
                ->get();
            if (!$show_students) {
                throw new Exception('No data available!');
            }

            return response()->json(array(
                'status' => true,
                'students' => $show_students,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $rules = array(
                'username'  => 'required',
                'password'     => 'required',
                'first_name'  => 'required',
                'last_name'   => 'required',
                'email'         => 'required',
                'phone'         => 'required',
                'address'       => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                throw new Exception('All fields are required');
            }

            $insert_student['username'] = $request->username;
            $insert_student['first_name'] = $request->first_name;
            $insert_student['last_name'] = $request->last_name;
            $insert_student['email'] = $request->email;
            $insert_student['phone'] = $request->phone;
            $insert_student['address'] = $request->address;
            $insert_student['role'] = "4";
            $insert_student['password'] = Hash::make($request->password);

            $create_student = User::create($insert_student);
            if (!$create_student) {
                throw new Exception('Create Student failed!');
            }

            return response()->json(array(
                'status' => true,
                'status_message' => "Student Create Successful!",
                'student' => $create_student,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $show_student = User::where('user_id', $id)
                ->first();
            if (!$show_student) {
                throw new Exception('Student doesnot exist!');
            }

            return response()->json(array(
                'status' => true,
                'student' => $show_student,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $rules = array(
                'username'  => 'required',
                'password'     => 'required',
                'first_name'  => 'required',
                'last_name'   => 'required',
                'email'         => 'required',
                'phone'         => 'required',
                'address'       => 'required',
            );
            $validator = Validator::make($request->all(), $rules);
            if (!$validator->passes()) {
                throw new Exception('All fields are required');
            }

            $update_student['username'] = $request->username;
            $update_student['first_name'] = $request->first_name;
            $update_student['last_name'] = $request->last_name;
            $update_student['email'] = $request->email;
            $update_student['phone'] = $request->phone;
            $update_student['address'] = $request->address;
            $update_student['password'] = Hash::make($request->password);

            $update = User::where('user_id', $id)
                ->update($update_student);
            if (!$update) {
                throw new Exception('Update Student failed!');
            }

            return response()->json(array(
                'status' => true,
                'status_message' => "Student Update Successful!",
                // 'teacher' => $update,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $delete_student = User::where('user_id', $id)
                ->delete();
            if (!$delete_student) {
                throw new Exception('Delete Student failed!');
            }

            return response()->json(array(
                'status' => true,
                'status_message' => "Student Delete Successful!",
                // 'teacher' => $delete_student,
            ));
        }
        catch (Exception $e) {
            return response()->json(array(
                'status' => false,
                'status_message' => $e->getMessage(),
            ));
        }          
    }
}
